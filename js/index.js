


(function() {
  let questionCount = 1;

  let  myQuestions = [ {

    question: "The improvement of computer hardware theory is summarized by which law.?",
    options: {

        a: "Metcalf's law",
        b: "Bill's Law",
        c: "Moore's First Law",
        d: "Grove's law"
    },

    answer: "c",
},
    {
        question: "The most widely used computer device is. ",

        options: {
            a: "Solid state disks",
            b: "External hard disk",
            c: "Internal hard disk",
            d: "Mouse"
        },

        answer: "c",
    },
    
    {
        question: "_______ are software which is used to do particular task.",

        options: {
            a: "Operating system",
            b: " Program",
            c: "Data",
            d:"Software"
        },

        answer: "b",
    },
    
    {
        question: "Who is father of modern computers?",

        options: {
            a: "Abraham Lincoln",
            b: "James Gosling",
            c: "Charles Babbage",
            d: "Gordon"
        },
    
        answer: "c",
    },
    
    {
        question: "How many generations of computers we have?",

        options: {
            a: "6",
            b: "7",
            c: "5",
            d: "4"
        },

        answer: "c",
    },
    
    {
        question: "________ controls the way in which the computer system functions and provides a means by which users can interact with the computer.",

        options: {
            a: "The operating system",
            b: "The motherboard",
            c: "The platform",
            d: "Application software"
        },

        answer: "a",
    },
    
    { 
        question: "The difference between people with access to computers and the Internet and those without this access is known as the:",

        options: {
            a: "digital divide.",
            b: "Internet divide.",
            c: "cyberway divide.",
            d: "Web divide."
        },

        answer: "a",
    },
    
    {
        question: "All of the following are examples of real security and privacy risks EXCEPT:",

        options: {
            a: "Viruses",
            b: "Hackers",
            c: "Spam",
            d: "Identity theft"
        },

        answer: "c",
    },
    
    {
        question: "The term 'Pentium' is related to",

        options: {
            a: "DVD",
            b: "Hard Disk",
            c: "Microprocessor",
            d: "Mouse"
        },

        answer: "c",
    },
    
    {
        question: "What does HTTP stands for?",

        options: {
            a: "Head Tail Transfer Protocol",
            b: "Hypertext Transfer Protocol",
            c: "Hypertext Transfer Plotter",
            d: "Hypertext Transfer Plot"
        },

        answer: "b",
    } ]



    function buildQuiz() {
      // we'll need a place to store the HTML output
      const output = [];
  
      // for each question...
      myQuestions.forEach((currentQuestion, questionNumber) => {
        // we'll want to store the list of answer choices
        const options = [];
  
        // and for each available answer...
        for (letter in currentQuestion.options) {
          // ...add an HTML radio button
          options.push(
            `<label>
               <input type="radio" name="question${questionNumber}" value="${letter}">
                ${letter} :
                ${currentQuestion.options[letter]}
             </label>`
          );
        }
  
        // add this question and its options to the output
        output.push(
          `<div class="slide">
             <div class="question"> ${currentQuestion.question} </div>
             <div class="answers"> ${options.join("")} </div>
           </div>`
        );
      });
  
      // finally combine our output list into one string of HTML and put it on the page
      quizContainer.innerHTML = output.join("");
    }
  
    function showResults() {
      // gather answer containers from our quiz
      const answerContainers = quizContainer.querySelectorAll(".answers");
  
      // keep track of user's answers
      let numCorrect = 0;
  
      // for each question...
      myQuestions.forEach((currentQuestion, questionNumber) => {
        // find selected answer
        const answerContainer = answerContainers[questionNumber];
        const selector = `input[name=question${questionNumber}]:checked`;
        const userAnswer = (answerContainer.querySelector(selector) || {}).value;
  
        // if answer is correct
        if (userAnswer === currentQuestion.answer) {
          // add to the number of correct answers
          numCorrect++;
  
          // color the answers green
          answerContainers[questionNumber].style.color = "lightgreen";
        } else {
          // if answer is wrong or blank
          // color the answers red
          answerContainers[questionNumber].style.color = "red";
        }
      });
  
      // show number of correct answers out of total
   
      if(numCorrect <= 5){
          resultsContainer.innerHTML = `You scored ${numCorrect} out of ${myQuestions.length}. Please retake the quiz `;
        
        }else{
          resultsContainer.innerHTML = `You scored ${numCorrect} out of ${myQuestions.length}. CONGRATULATIONS!!`;
        }

    }
  
    function showSlide(n) {
      slides[currentSlide].classList.remove("active-slide");
      slides[n].classList.add("active-slide");
      currentSlide = n;
      
      if (currentSlide === 0) {
        previousButton.style.display = "none";
      } else {
        previousButton.style.display = "inline-block";
      }
      
      if (currentSlide === slides.length - 1) {
        nextButton.style.display = "none";
        submitButton.style.display = "inline-block";
      } else {
        nextButton.style.display = "inline-block";
        submitButton.style.display = "none";
      }
    }
  
    function showNextSlide() {
      showSlide(currentSlide + 1);
      questionCount++;
      count.innerHTML = `Question ${questionCount} of  ${myQuestions.length} `;
      
    }
  
    function showPreviousSlide() {
      showSlide(currentSlide - 1);
      questionCount--;
      count.innerHTML = `Question ${questionCount} of  ${myQuestions.length} `;
    }
  
    const quizContainer = document.getElementById("quiz");
    const resultsContainer = document.getElementById("results");
    const submitButton = document.getElementById("submit");
  
    // display quiz right away
    buildQuiz();
  
    const previousButton = document.getElementById("previous");
    const nextButton = document.getElementById("next");
    const slides = document.querySelectorAll(".slide");
    let currentSlide = 0;
    count.innerHTML =  `Question ${questionCount} of  ${myQuestions.length} `;
  
    showSlide(0);
  
    // on submit, show results
    submitButton.addEventListener("click", showResults);
    previousButton.addEventListener("click", showPreviousSlide);
    nextButton.addEventListener("click", showNextSlide);
  })();